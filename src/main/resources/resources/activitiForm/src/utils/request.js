/*
 * @Author: 郑锐 307057322@qq.com
 * @Date: 2022-08-06 09:06:17
 * @LastEditors: 郑锐 307057322@qq.com
 * @LastEditTime: 2022-10-21 09:42:31
 * @FilePath: \DEMO-EnterpriseElectricityIntelligentWeb\src\utils\request.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/**
 * @description axios 请求封装
 * @author zr
 */
import axios from 'axios'
import { MessageBox, Message } from 'element-ui'

// 创建axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 90000
})
service.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

// 'application/json'
// 请求拦截器
service.interceptors.request.use(
  (config) => {
    const { url } = config
    if (url.indexOf('login') > -1 || url.indexOf('logout') > -1) {
      return config
    } else {
      return config
    }
  },
  (error) => {
    // 请求发生错误
    console.log(error)
    return Promise.reject(error)
  }
)
// 响应拦截器
service.interceptors.response.use(
  (response) => {
    const res = response.data
    if (response.status === 200) {
      if (res.status !== 0) {
        return Message({
          message: res.msg || 'Error',
          type: 'error',
          duration: 5 * 1000
        })
      } else {
        return Promise.resolve(res)
      }
    } else {
      return Promise.reject(new Error(res.msg))
    }
  },
  (error) => {
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
