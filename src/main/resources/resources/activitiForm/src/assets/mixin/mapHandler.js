import fullscreen from 'vue-fullscreen'
import Vue from 'vue'
import { loadModules } from 'esri-loader'
import { mapState } from 'vuex'
import mapTools from '@/components/common/mapTools'
import { initTDTLayer, mapAndCenter } from '@/utils/map/mapTools3x'
Vue.use(fullscreen)
export default {
  components: {
    mapTools
  },
  data() {
    return {
      map: null,
      center: false,
      mapBaseInfo: {
        center: [120.441735, 31.334343],
        zoom: 11,
        logo: false,
        slider: false
      },
      fullscreen: false,
      currentType: '卫星图',
      loadModulesArr: JSON.parse(sessionStorage.getItem('loadModulesArr')),
      options: JSON.parse(sessionStorage.getItem('options')),
      mapPointList: [
        {
          jd: 120.441735,
          wd: 31.334343,
          type: 'AIR_STATION',
          title: '大轮浜横山路北',
          id: 101
        },
        {
          jd: 120.451735,
          wd: 31.34343,
          type: 'AIR_STATION',
          title: '测试点位2',
          id: 42
        },
        {
          jd: 120.454735,
          wd: 31.38343,
          type: 'AIR_STATION',
          title: '测试点位3',
          id: 41
        },
        {
          jd: 120.461735,
          wd: 31.35343,
          type: 'AIR_STATION',
          title: '测试点位4',
          id: -1
        }
      ],
      mapHoverData: {}
    }
  },
  computed: {
    ...mapState({
      mapModular: (state) => state.mapModular.mapModular
    })
  },
  mounted() {
    this.loadModules()
  },
  methods: {
    // 加载地图包
    loadModules() {
      loadModules(this.loadModulesArr, this.options).then(() => {
        this.$nextTick(this.gisInit)
      })
    },
    // 全屏方法
    fullScreen() {
      this.$fullscreen.toggle(this.$el.querySelector('.map-container-warp'), {
        wrap: false,
        callback: this.fullscreenChange
      })
    },
    fullscreenChange(fullscreen) {
      this.fullscreen = fullscreen
    },
    // 切换行政区图层
    changeCurrentType(data) {
      initTDTLayer(this.map, data)
    },
    // 切换中心点
    changeCenter() {
      mapAndCenter(
        this.mapBaseInfo.center[0],
        this.mapBaseInfo.center[1],
        this.map,
        this.mapBaseInfo.zoom
      )
    }
  }
}
